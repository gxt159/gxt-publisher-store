using GxPublisherStore.Entities;
using GxPublisherStore.Exceptions;
using GxPublisherStore.Models.Input;
using GxPublisherStore.Models.Output;
using GxPublisherStore.Services;
using Microsoft.EntityFrameworkCore;
using Moq;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Xunit;

namespace PublisherStore.Tests.Tests.Unit.Publisher.Services
{
    public class PublisherServiceTest
    {
        [Fact]
        public async Task When_PublisherHasNoGames_ReturnEmptyList()
        {
            // Arrange.
            var publisherEntity1 = new PublisherEntity
            {
                Id = Guid.NewGuid(),
                PublisherName = "Test1",
                Money = 100,
            };

            var publisherEntity2 = new PublisherEntity
            {
                Id = Guid.NewGuid(),
                PublisherName = "Test2",
                Money = 0,
            };

            var gameEntity1 = new GameEntity
            {
                Id = Guid.NewGuid(),
                Publisher = publisherEntity1,
                PublisherId = publisherEntity1.Id,
            };

            var gameEntity2 = new GameEntity
            {
                Id = Guid.NewGuid(),
                Publisher = publisherEntity1,
                PublisherId = publisherEntity1.Id,
            };

            var store = new List<StoreEntity>
            {
                new StoreEntity
                {
                    Id = Guid.NewGuid(),
                    Game = gameEntity1,
                    GameId = gameEntity1.Id,
                    Cost = 500,
                },
                new StoreEntity
                {
                    Id = Guid.NewGuid(),
                    Game = gameEntity2,
                    GameId = gameEntity2.Id,
                    Cost = 500,
                },
            };

            var expected = new List<GameStoreOutputModel>();

            var publisher = new PublisherInputModel()
            {
                Id = publisherEntity2.Id.ToString(),
            };

            var mockStoreDbSet = GetQueryableMockDbSet(store);
            var mockContext = new Mock<GameDbContext>();

            // Arrange behavior.
            mockContext.Setup(x => x.Store).Returns(mockStoreDbSet);

            // Arrange SUT.
            var publisherService = new PublisherService(mockContext.Object);


            // Act.
            var actual = await publisherService.GetGames(publisher);

            // Assert.
            Assert.Equal(expected, actual);
        }

        [Fact]
        public async Task When_PublisherHasGames_ReturnListWithGames()
        {
            // Arrange.
            var publisherEntity = new PublisherEntity
            {
                Id = Guid.NewGuid(),
                Money = 100,
                PublisherName = "test",
            };

            var gameEntity1 = new GameEntity
            {
                Id = Guid.NewGuid(),
                Publisher = publisherEntity,
                PublisherId = publisherEntity.Id,
                Name = "Test game",
                Source = "test src",
            };

            var gameEntity2 = new GameEntity
            {
                Id = Guid.NewGuid(),
                Publisher = publisherEntity,
                PublisherId = publisherEntity.Id,
                Name = "Test game 2",
                Source = "test src",
            };

            var store = new List<StoreEntity>
            {
                new StoreEntity
                {
                    Id = Guid.NewGuid(),
                    Game = gameEntity1,
                    GameId = gameEntity1.Id,
                    Cost = 300,
                },
                new StoreEntity
                {
                    Id = Guid.NewGuid(),
                    Game = gameEntity2,
                    GameId = gameEntity2.Id,
                    Cost = 500,
                }
            };

            var expected = store.Select(x => new GameStoreOutputModel
            {
                Id = x.Game.Id.ToString(),
                Name = x.Game.Name,
                Cost = x.Cost,
                Source = x.Game.Source
            }).ToList();

            var publisher = new PublisherInputModel()
            {
                Id = publisherEntity.Id.ToString(),
            };

            var mockStoreSet = GetQueryableMockDbSet(store);
            var mockContext = new Mock<GameDbContext>();

            // Arrange behavior.
            mockContext.Setup(x => x.Store).Returns(mockStoreSet);

            // Arrange SUT.
            var publisherService = new PublisherService(mockContext.Object);


            // Act.
            var actual = await publisherService.GetGames(publisher);

            // Assert.
            Assert.Equal(expected.Count, actual.Count);
        }

        [Fact]
        public async Task When_PublisherAddsNewGame_ReturnAddedNewGame()
        {
            // Arrange.
            var games = new List<GameEntity>();

            var publisherEntity = new PublisherEntity
            {
                Id = Guid.NewGuid(),
                Money = 500,
                PublisherName = "Test",
            };

            var gameEntity = new GameEntity
            {
                Id = Guid.NewGuid(),
                PublisherId = publisherEntity.Id,
            };

            var storeEntity = new StoreEntity
            {
                Id = Guid.NewGuid(),
                Game = gameEntity,
                GameId = gameEntity.Id,
            };

            var inputGame = new GameInputModel
            {
                Name = gameEntity.Name,
                Source = gameEntity.Source,
                Cost = storeEntity.Cost,
            };

            var expected = new GameStoreOutputModel
            {
                Id = Guid.Empty.ToString(),
                Name = gameEntity.Name,
                Cost = storeEntity.Cost,
                Source = gameEntity.Source
            };

            var publisher = new PublisherInputModel()
            {
                Id = publisherEntity.Id.ToString(),
            };

            var mockDbSet = GetQueryableMockDbSet(games);
            var mockContext = new Mock<GameDbContext>();

            // Arrange behavior.
            mockContext.Setup(x => x.Games).Returns(mockDbSet);

            // Arrange SUT.
            var publisherService = new PublisherService(mockContext.Object);

            // Act.
            var actual = await publisherService.CreateGame(publisher, inputGame);

            // Assert.
            Assert.Equal(expected.Id, actual.Id);
            Assert.Equal(expected.Name, actual.Name);
            Assert.Equal(expected.Cost, actual.Cost);
            Assert.Equal(expected.Source, actual.Source);
        }

        [Fact]
        public async Task When_PublisherRequestsExistingAndHisOwnGame_ReturnRequestedGame()
        {
            // Arrange.
            var publisherEntity = new PublisherEntity
            {
                Id = Guid.NewGuid(),
                Money = 500,
                PublisherName = "Test",
            };

            var gameEntity = new GameEntity
            {
                Id = Guid.NewGuid(),
                PublisherId = publisherEntity.Id,
            };

            var storeEntity = new StoreEntity
            {
                Id = Guid.NewGuid(),
                Game = gameEntity,
                GameId = gameEntity.Id,
            };

            var store = new List<StoreEntity>
            {
                storeEntity,
            };

            var expected = new GameStoreOutputModel
            {
                Id = storeEntity.Id.ToString(),
                Name = gameEntity.Name,
                Source = gameEntity.Source,
                Cost = storeEntity.Cost
            };

            var publisher = new PublisherInputModel()
            {
                Id = publisherEntity.Id.ToString(),
            };

            var mockStoreDbSet = GetQueryableMockDbSet(store);
            var mockContext = new Mock<GameDbContext>();

            // Arrange behavior.
            mockContext.Setup(x => x.Store).Returns(mockStoreDbSet);

            // Arrange SUT.
            var publisherService = new PublisherService(mockContext.Object);

            // Act.
            var actual = await publisherService.GetGame(publisher, gameEntity.Id.ToString());

            // Assert.
            Assert.Equal(expected.Id, actual.Id);
            Assert.Equal(expected.Name, actual.Name);
            Assert.Equal(expected.Cost, actual.Cost);
            Assert.Equal(expected.Source, actual.Source);
        }

        [Fact]
        public async Task When_PublisherTriesToGetNonExistingGame_ReturnNotExistingGameException()
        {
            // Arrange.
            var publisherEntity = new PublisherEntity
            {
                Id = Guid.NewGuid(),
                PublisherName = "Test",
                Money = 500,
            };

            var storeEntity = new StoreEntity
            {
                Id = Guid.NewGuid(),
            };

            var store = new List<StoreEntity>();

            var publisher = new PublisherInputModel()
            {
                Id = publisherEntity.Id.ToString(),
            };

            var mockStoreDbSet = GetQueryableMockDbSet(store);
            var mockContext = new Mock<GameDbContext>();

            // Arrange behavior.
            mockContext.Setup(x => x.Store).Returns(mockStoreDbSet);

            // Arrange SUT.
            var publisherService = new PublisherService(mockContext.Object);

            // Assert.
            await Assert.ThrowsAsync<NotExistingGameException>(() => publisherService.GetGame(publisher, storeEntity.Id.ToString()));
        }

        [Fact]
        public async Task When_PublisherTriesToGetGameOfOtherPublisher_ReturnForbiddenException()
        {
            // Arrange.
            var publisherEntity1 = new PublisherEntity
            {
                Id = Guid.NewGuid(),
                Money = 500,
                PublisherName = "Test1",
            };

            var publisherEntity2 = new PublisherEntity
            {
                Id = Guid.NewGuid(),
                Money = 500,
                PublisherName = "Test2",
            };

            var gameEntity = new GameEntity
            {
                Id = Guid.NewGuid(),
                PublisherId = publisherEntity2.Id,
            };

            var storeEntity = new StoreEntity
            {
                Id = Guid.NewGuid(),
                Game = gameEntity,
                GameId = gameEntity.Id,
                Cost = 500,
            };

            var store = new List<StoreEntity>
            {
                storeEntity,
            };

            var publisher = new PublisherInputModel()
            {
                Id = publisherEntity1.Id.ToString(),
            };

            var mockStoreDbSet = GetQueryableMockDbSet(store);
            var mockContext = new Mock<GameDbContext>();

            // Arrange behavior.
            mockContext.Setup(x => x.Store).Returns(mockStoreDbSet);

            // Arrange SUT.
            var publisherService = new PublisherService(mockContext.Object);

            // Assert.
            await Assert.ThrowsAsync<ForbiddenException>(() => publisherService.GetGame(publisher, gameEntity.Id.ToString()));
        }

        [Fact]
        public async Task When_PublisherSendsInvalidGameId_ReturnNotGuidException()
        {
            // Arrange.
            var publisherEntity = new PublisherEntity
            {
                Id = Guid.NewGuid(),
                Money = 500,
                PublisherName = "Test",
            };

            var games = new List<GameEntity>();

            var publisher = new PublisherInputModel()
            {
                Id = publisherEntity.Id.ToString(),
            };

            var mockDbSet = GetQueryableMockDbSet(games);
            var mockContext = new Mock<GameDbContext>();

            // Arrange behavior.
            mockContext.Setup(x => x.Games).Returns(mockDbSet);

            // Arrange SUT.
            var publisherService = new PublisherService(mockContext.Object);

            // Assert.
            await Assert.ThrowsAsync<NotGuidException>(() => publisherService.GetGame(publisher, "abcde"));
        }

        private static DbSet<T> GetQueryableMockDbSet<T>(List<T> sourceList) where T : class
        {
            var queryable = sourceList.AsQueryable();

            var dbSet = new Mock<DbSet<T>>();
            dbSet.As<IQueryable<T>>().Setup(m => m.Provider).Returns(queryable.Provider);
            dbSet.As<IQueryable<T>>().Setup(m => m.Expression).Returns(queryable.Expression);
            dbSet.As<IQueryable<T>>().Setup(m => m.ElementType).Returns(queryable.ElementType);
            dbSet.As<IQueryable<T>>().Setup(m => m.GetEnumerator()).Returns(() => queryable.GetEnumerator());

            return dbSet.Object;
        }
    }
}
