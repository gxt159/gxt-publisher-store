using GxPublisherStore.Services;
using Microsoft.AspNetCore.TestHost;
using Microsoft.Extensions.Hosting;
using Microsoft.Extensions.DependencyInjection;
using System;
using System.Collections.Generic;
using System.Text;
using System.Threading.Tasks;
using Xunit;
using Microsoft.AspNetCore.Hosting;
using Microsoft.AspNetCore.Builder;
using GxPublisherStore.Middleware;
using GxPublisherStore.Models.Input;
using Microsoft.AspNetCore.Http;
using System.IO;
using Newtonsoft.Json;
using System.Net.Http;
using GxPublisherStore.Exceptions;

namespace PublisherStore.Tests.Tests.Unit.Publisher.Middlewares
{
    public class AccessMiddlewareTest
    {
        private async Task<IHost> CreateHostBuilder()
        {
            return await new HostBuilder()
                .ConfigureWebHost(webBuilder =>
                {
                    webBuilder
                        .UseTestServer()
                        .ConfigureServices(services =>
                        {
                            services.AddScoped<PublisherService>();
                        })
                        .Configure(app =>
                        {
                            app.UseMiddleware<AccessMiddleware>();
                        });
                })
                .StartAsync();
        }


        [Fact]
        public async Task When_UserIsNotPublisher_ReturnAccessForbiddenException()
        {
            // Arrange.
            var publisher = new PublisherInputModel()
            {
                Id = "1"
            };

            var accessToken = "eyJhbGciOiJIUzI1NiIsInR5cCI6IkpXVCJ9.eyJpc1B1Ymxpc2hlciI6ZmFsc2V9.Liu9dosvQk70LS0O3dvwi_F5Tw_LO3_UjW33pDbAl2Y";

            using var host = CreateHostBuilder().Result;

            var client = host.GetTestClient();


            var request = new HttpRequestMessage
            {
                Method = HttpMethod.Get,
                RequestUri = new Uri("http://localhost/publisher"),
                Content = new StringContent(JsonConvert.SerializeObject(publisher), Encoding.UTF8, "application/json")
            };

            request.Headers.Add("AccessToken", accessToken);

            // Assert.
            await Assert.ThrowsAsync<ForbiddenException>(() => client.SendAsync(request));
        }

        [Fact]
        public async Task When_UserIsPublisher_PassMiddleware()
        {
            var publisher = new PublisherInputModel()
            {
                Id = "1"
            };

            var accessToken = "eyJhbGciOiJIUzI1NiIsInR5cCI6IkpXVCJ9.eyJpc1B1Ymxpc2hlciI6dHJ1ZX0.Oyoyl1rP5SXc6aCcRNt8xhaOvQgtLc9uL6826RR62Yo";

            using var host = CreateHostBuilder().Result;

            var client = host.GetTestClient();


            var request = new HttpRequestMessage
            {
                Method = HttpMethod.Get,
                RequestUri = new Uri("http://localhost/publisher"),
                Content = new StringContent(JsonConvert.SerializeObject(publisher), Encoding.UTF8, "application/json")
            };

            request.Headers.Add("AccessToken", accessToken);

            var response = await client.SendAsync(request);
        }
    }
}
