using GxPublisherStore.Controllers;
using GxPublisherStore.Models.Input;
using GxPublisherStore.Models.Output;
using GxPublisherStore.Services;
using Moq;
using System;
using System.Collections.Generic;
using System.Text;
using System.Threading.Tasks;
using Xunit;

namespace PublisherStore.Tests.Tests.Unit.Publisher.PublisherControllers
{
    public class PublisherControllerTest
    {
        [Fact]
        public async Task When_PublisherHasGotGames_ReturnGamesList()
        {
            // Arrange.
            var game = new GameStoreOutputModel
            {
                Id = "auf",
                Name = "aa",
                Source = "dd",
                Cost = 228
            };

            var games = new List<GameStoreOutputModel>
            {
                game
            };

            var expected = new List<GameStoreOutputModel>
            {
                new GameStoreOutputModel
                {
                    Id = "auf",
                    Name = "aa",
                    Source = "dd",
                    Cost = 228
                }
            };

            var publisher = new PublisherInputModel()
            {
                Id = "1"
            };

            var mockService = new Mock<IPublisherService>();

            // Arrange behavior.
            mockService.Setup(x => x.GetGames(publisher)).ReturnsAsync(games);


            // Arrange SUT.
            var publisherController = new PublisherController(mockService.Object);


            // Act.
            var actual = await publisherController.GetGames(publisher);

            // Assert.
            Assert.Equal(expected.Count, actual.Count);
        }

        [Fact]
        public async Task When_PublisherHasCreatedGame_ReturnCreatedGame()
        {
            // Arrange.
            var game = new GameInputModel
            {
                Name = "aa",
                Source = "dd",
                Cost = 228
            };

            var publisher = new PublisherInputModel()
            {
                Id = "1"
            };

            var input = new PublisherGameInputModel
            {
                Game = game,
                Publisher = publisher
            };

            var games = new List<GameStoreOutputModel>();

            var expected = new GameStoreOutputModel
            {
                Id = "auf",
                Name = "aa",
                Source = "dd",
                Cost = 228
            };
            var mockService = new Mock<IPublisherService>();

            // Arrange behavior.
            mockService.Setup(x => x.CreateGame(publisher, game)).ReturnsAsync(expected);


            // Arrange SUT.
            var publisherController = new PublisherController(mockService.Object);


            // Act.
            var actual = await publisherController.CreateGame(input);

            // Assert.
            Assert.Equal(expected.Id, actual.Id);
            Assert.Equal(expected.Name, actual.Name);
            Assert.Equal(expected.Cost, actual.Cost);
            Assert.Equal(expected.Source, actual.Source);
        }

        [Fact]
        public async Task When_PublisherHasGotGame_ReturnGame()
        {
            // Arrange.
            var id = "whatever";

            var publisher = new PublisherInputModel()
            {
                Id = "1"
            };

            var expected = new GameStoreOutputModel
            {
                Id = "auf",
                Name = "aa",
                Source = "dd",
                Cost = 228
            };
            var mockService = new Mock<IPublisherService>();

            // Arrange behavior.
            mockService.Setup(x => x.GetGame(publisher, id)).ReturnsAsync(expected);


            // Arrange SUT.
            var publisherController = new PublisherController(mockService.Object);


            // Act.
            var actual = await publisherController.GetGame(publisher, id);

            // Assert.
            Assert.Equal(expected.Id, actual.Id);
            Assert.Equal(expected.Name, actual.Name);
            Assert.Equal(expected.Cost, actual.Cost);
            Assert.Equal(expected.Source, actual.Source);
        }
    }
}
