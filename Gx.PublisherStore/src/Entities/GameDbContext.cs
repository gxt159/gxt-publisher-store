namespace GxPublisherStore.Entities
{
    using System;
    using System.Collections.Generic;
    using System.Linq;
    using System.Threading.Tasks;
    using GxPublisherStore.Extensions;
    using Microsoft.EntityFrameworkCore;
    using Microsoft.Extensions.Configuration;

    /// <summary>
    /// Database context of the application.
    /// </summary>
    public class GameDbContext : DbContext
    {
        /// <summary>
        /// Initializes a new instance of the <see cref="GameDbContext"/> class.
        /// </summary>
        public GameDbContext()
        {
        }

        /// <summary>
        /// Initializes a new instance of the <see cref="GameDbContext"/> class.
        /// </summary>
        /// <param name="configuration">Gathered configuration object.</param>
        public GameDbContext(IConfiguration configuration)
        {
            ServerAddress = configuration["DbConfig:ServerAddress"];
            User = configuration["DbConfig:User"];
            Password = configuration["DbConfig:Password"];
            Port = configuration["DbConfig:Port"];
        }

        /// <summary>
        /// Gets or sets database set of <see cref="GameEntity"/>.
        /// </summary>
        /// <value>
        /// Database set of <see cref="GameEntity"/>.
        /// </value>
        public virtual DbSet<GameEntity> Games { get; set; }

        /// <summary>
        /// Gets or sets database set of <see cref="StoreEntity"/>.
        /// </summary>
        /// <value>
        /// Database set of <see cref="StoreEntity"/>.
        /// </value>
        public virtual DbSet<StoreEntity> Store { get; set; }

        /// <summary>
        /// Gets or sets database set of <see cref="PublisherEntity"/>.
        /// </summary>
        /// <value>
        /// Database set of <see cref="PublisherEntity"/>.
        /// </value>
        public virtual DbSet<PublisherEntity> Publishers { get; set; }

        private string User { get; }

        private string Password { get; }

        private string ServerAddress { get; }

        private string Port { get; }

        /// <summary>
        /// Gets games array by publisher's ID.
        /// </summary>
        /// <param name="id">Publisher's ID.</param>
        /// <returns>Returns <see cref="StoreEntity"/> list.</returns>
        public async Task<List<StoreEntity>> GetGamesByPublisherId(string id)
        {
            return await Store.Where(x => x.Game.PublisherId == Guid.Parse(id))
                .Include(x => x.Game)
                .ToListAsyncSafe();
        }

        /// <summary>
        /// Gets publisher's game by game's guid.
        /// </summary>
        /// <param name="guid">Game's guid.</param>
        /// <returns>Returns founded <see cref="StoreEntity"/> or null.</returns>
        public async Task<StoreEntity> GetGameByGuid(Guid guid)
        {
            return await Store.SingleOrDefaultAsyncSafe(x => x.GameId == guid);
        }

        /// <inheritdoc/>
        protected override void OnConfiguring(DbContextOptionsBuilder optionsBuilder)
        {
            optionsBuilder.UseNpgsql($"Server={ServerAddress};Port={Port};Database=gx-db;Username={User};Password={Password}");
        }
    }
}
