namespace GxPublisherStore.Entities
{
    using System;

    /// <summary>
    /// Represemts store entity in database.
    /// </summary>
    public class StoreEntity
    {
        /// <summary>
        /// Gets or sets store ID.
        /// </summary>
        /// <value>
        /// Store ID.
        /// </value>
        public Guid Id { get; set; }

        /// <summary>
        /// Gets or sets game foreign key.
        /// </summary>
        /// <value>
        /// Game foreign key.
        /// </value>
        public Guid GameId { get; set; }

        /// <summary>
        /// Gets or sets game relation.
        /// </summary>
        /// <value>
        /// Game relation.
        /// </value>
        public virtual GameEntity Game { get; set; }

        /// <summary>
        /// Gets or sets game cost in store.
        /// </summary>
        /// <value>
        /// Game cost in store.
        /// </value>
        public decimal Cost { get; set; }
    }
}
