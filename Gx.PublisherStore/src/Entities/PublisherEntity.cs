namespace GxPublisherStore.Entities
{
    using System;
    using System.Collections.Generic;

    /// <summary>
    /// Represents publisher entity storing in database.
    /// </summary>
    public class PublisherEntity
    {
        /// <summary>
        /// Initializes a new instance of the <see cref="PublisherEntity"/> class.
        /// </summary>
        public PublisherEntity()
        {
            Games = new List<GameEntity>();
        }

        /// <summary>
        /// Gets or sets publisher id.
        /// </summary>
        /// <value>
        /// Publisher id.
        /// </value>
        public Guid Id { get; set; }

        /// <summary>
        /// Gets or sets publisher name.
        /// </summary>
        /// <value>
        /// Publisher name.
        /// </value>
        public string PublisherName { get; set; }

        /// <summary>
        /// Gets or sets publisher money.
        /// </summary>
        /// <value>
        /// Publisher money.
        /// </value>
        public decimal Money { get; set; }

        /// <summary>
        /// Gets or sets collection of games.
        /// </summary>
        /// <value>
        /// Collection of games.
        /// </value>
        public ICollection<GameEntity> Games { get; set; }
    }
}
