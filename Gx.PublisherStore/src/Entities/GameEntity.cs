namespace GxPublisherStore.Entities
{
    using System;
    using System.Collections.Generic;

    /// <summary>
    /// Represents game entity storing in database.
    /// </summary>
    public class GameEntity
    {
        /// <summary>
        /// Initializes a new instance of the <see cref="GameEntity"/> class.
        /// </summary>
        public GameEntity()
        {
            Stores = new List<StoreEntity>();
        }

        /// <summary>
        /// Gets or sets game's guid.
        /// </summary>
        /// <value>
        /// Represents game's guid.
        /// </value>
        public Guid Id { get; set; }

        /// <summary>
        /// Gets or sets publisher's ID.
        /// </summary>
        /// <value>
        /// Publisher's ID.
        /// </value>
        public Guid PublisherId { get; set; }

        /// <summary>
        /// Gets or sets publisher relation.
        /// </summary>
        /// <value>
        /// Publisher relation.
        /// </value>
        public PublisherEntity Publisher { get; set; }

        /// <summary>
        /// Gets or sets store collection.
        /// </summary>
        /// <value>
        /// Store collection.
        /// </value>
        public virtual ICollection<StoreEntity> Stores { get; set; }

        /// <summary>
        /// Gets or sets game's name.
        /// </summary>
        /// <value>
        /// Game's name.
        /// </value>
        public string Name { get; set; }

        /// <summary>
        /// Gets or sets game's source.
        /// </summary>
        /// <value>
        /// Game's source.
        /// </value>
        public string Source { get; set; }
    }
}
