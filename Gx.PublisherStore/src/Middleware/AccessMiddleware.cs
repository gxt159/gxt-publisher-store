namespace GxPublisherStore.Middleware
{
    using System.IdentityModel.Tokens.Jwt;
    using System.Linq;
    using System.Threading.Tasks;
    using GxPublisherStore.Exceptions;
    using Microsoft.AspNetCore.Http;

    /// <summary>
    /// Middleware checking access to publisher's services.
    /// </summary>
    public class AccessMiddleware
    {
        private readonly RequestDelegate next;

        /// <summary>
        /// Initializes a new instance of the <see cref="AccessMiddleware"/> class.
        /// </summary>
        /// <param name="next">Defines HTTP requests.</param>
        public AccessMiddleware(RequestDelegate next)
        {
            this.next = next;
        }

        /// <summary>
        /// Invokes ckeck of user's rights.
        /// </summary>
        /// <param name="context">HTTP context.</param>
        /// <returns>Raise <see cref="ForbiddenException"/> in case of failed check.</returns>
        public async Task InvokeAsync(HttpContext context)
        {
            var accessToken = context.Request.Headers["AccessToken"];

            var stream = accessToken;

            if (stream.Count == 0)
            {
                await next.Invoke(context);
            }
            else
            {
                var handler = new JwtSecurityTokenHandler();
                var tokenS = handler.ReadToken(stream) as JwtSecurityToken;

                var isPublisher = bool.Parse(tokenS.Claims.SingleOrDefault(x => x.Type == "isPublisher").Value);

                if (!isPublisher)
                {
                    throw new ForbiddenException();
                }
                else
                {
                    await next.Invoke(context);
                }
            }
        }
    }
}
