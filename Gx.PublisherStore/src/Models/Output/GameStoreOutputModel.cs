namespace GxPublisherStore.Models.Output
{
    /// <summary>
    /// Represents game model for publisher's actions.
    /// </summary>
    public class GameStoreOutputModel
    {
        /// <summary>
        /// Gets or sets store ID in database.
        /// </summary>
        /// <value>
        /// Store ID in database.
        /// </value>
        public string Id { get; set; }

        /// <summary>
        /// Gets or sets game's name.
        /// </summary>
        /// <value>
        /// Game's name.
        /// </value>
        public string Name { get; set; }

        /// <summary>
        /// Gets or sets game's cost.
        /// </summary>
        /// <value>
        /// Game's cost.
        /// </value>
        public decimal Cost { get; set; }

        /// <summary>
        /// Gets or sets game's source (it can be link/file/etc).
        /// </summary>
        /// <value>
        /// Game's source (it can be link/file/etc).
        /// </value>
        public string Source { get; set; }
    }
}
