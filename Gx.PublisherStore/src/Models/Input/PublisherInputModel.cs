namespace GxPublisherStore.Models.Input
{
    using System.ComponentModel.DataAnnotations;

    /// <summary>
    /// Represents publisher's model for publisher's actions.
    /// </summary>
    public class PublisherInputModel
    {
        /// <summary>
        /// Gets or sets publisher's ID.
        /// </summary>
        /// <value>
        /// Publisher's ID.
        /// </value>
        [Required]
        public string Id { get; set; }
    }
}
