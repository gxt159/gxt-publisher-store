namespace GxPublisherStore.Models.Input
{
    using System.ComponentModel.DataAnnotations;
    using System.Diagnostics.CodeAnalysis;

    /// <summary>
    /// Represents game model for publisher's actions.
    /// </summary>
    public class GameInputModel
    {
        /// <summary>
        /// Gets or sets game's name.
        /// </summary>
        /// <value>
        /// Game's name.
        /// </value>
        [Required]
        [NotNull]
        public string Name { get; set; }

        /// <summary>
        /// Gets or sets game's cost.
        /// </summary>
        /// <value>
        /// Game's cost.
        /// </value>
        [Required]
        [NotNull]
        public decimal Cost { get; set; }

        /// <summary>
        /// Gets or sets game's source (it can be link/file/etc).
        /// </summary>
        /// <value>
        /// Game's source (it can be link/file/etc).
        /// </value>
        public string Source { get; set; }
    }
}
