namespace GxPublisherStore.Models.Input
{
    using System.ComponentModel.DataAnnotations;
    using System.Diagnostics.CodeAnalysis;

    /// <summary>
    /// Represents publisher's and game's model for publisher's actions.
    /// </summary>
    public class PublisherGameInputModel
    {
        /// <summary>
        /// Gets or sets <see cref="PublisherInputModel"/>.
        /// </summary>
        /// <value>
        /// <see cref="PublisherInputModel"/>.
        /// </value>
        [Required]
        [NotNull]
        public PublisherInputModel Publisher { get; set; }

        /// <summary>
        /// Gets or sets <see cref="GameInputModel"/>.
        /// </summary>
        /// <value>
        /// <see cref="GameInputModel"/>.
        /// </value>
        [Required]
        [NotNull]
        public GameInputModel Game { get; set; }
    }
}
