namespace GxPublisherStore.Services
{
    using System;
    using System.Collections.Generic;
    using System.Linq;
    using System.Threading.Tasks;
    using GxPublisherStore.Models.Input;
    using GxPublisherStore.Models.Output;

    /// <summary>
    /// Service responsible for users' account actions.
    /// </summary>
    public interface IPublisherService
    {
        /// <summary>
        /// Get games from publisher store.
        /// </summary>
        /// <param name="publisher">Defines publisher input model.</param>
        /// <returns>A <see cref="Task{TResult}"/> representing the result of the asynchronous operation.</returns>
        public Task<List<GameStoreOutputModel>> GetGames(PublisherInputModel publisher);

        /// <summary>
        /// Create game with publisher and game models.
        /// </summary>
        /// <param name="publisher">Publisher model with publisher data.</param>
        /// <param name="gameInputModel">Game model with cost.</param>
        /// <returns>A <see cref="Task{TResult}"/> representing the result of the asynchronous operation.</returns>
        public Task<GameStoreOutputModel> CreateGame(PublisherInputModel publisher, GameInputModel gameInputModel);

        /// <summary>
        /// Get specific game from publisher store.
        /// </summary>
        /// <param name="publisher">Defines publisher input model.</param>
        /// <param name="id">Defines game ID.</param>
        /// <returns>A <see cref="Task{TResult}"/> representing the result of the asynchronous operation.</returns>
        public Task<GameStoreOutputModel> GetGame(PublisherInputModel publisher, string id);
    }
}
