namespace GxPublisherStore.Services
{
    using System;
    using System.Collections.Generic;
    using System.Linq;
    using System.Threading.Tasks;
    using GxPublisherStore.Entities;
    using GxPublisherStore.Exceptions;
    using GxPublisherStore.Models.Input;
    using GxPublisherStore.Models.Output;

    /// <summary>
    /// Represents publisher's services.
    /// </summary>
    public class PublisherService : IPublisherService
    {
        /// <summary>
        /// Initializes a new instance of the <see cref="PublisherService"/> class.
        /// </summary>
        /// <param name="context">Context of games database.</param>
        public PublisherService(GameDbContext context)
        {
            Context = context;
        }

        private GameDbContext Context { get; }

        /// <summary>
        /// Gets games by publisher's ID.
        /// </summary>
        /// <param name="publisher">Represents publisher's model.</param>
        /// <returns>Returns founded games.</returns>
        public async Task<List<GameStoreOutputModel>> GetGames(PublisherInputModel publisher)
        {
            var games = await Context.GetGamesByPublisherId(publisher.Id);
            return games.Select(x => new GameStoreOutputModel { Id = x.Id.ToString(), Name = x.Game.Name, Cost = x.Cost, Source = x.Game.Source }).ToList();
        }

        /// <summary>
        /// Creates inputed game by publisher.
        /// </summary>
        /// <param name="publisher">Represents publisher's model.</param>
        /// <param name="gameInputModel">Represents game's model.</param>
        /// <returns>Returns created game.</returns>
        public async Task<GameStoreOutputModel> CreateGame(PublisherInputModel publisher, GameInputModel gameInputModel)
        {
            var game = new GameEntity()
            {
                Id = default,
                Name = gameInputModel.Name,
                Source = gameInputModel.Source,
                PublisherId = Guid.Parse(publisher.Id),
            };

            var store = new StoreEntity
            {
                Id = default,
                Game = game,
                Cost = gameInputModel.Cost,
            };

            await Context.AddAsync(game);
            await Context.AddAsync(store);
            await Context.SaveChangesAsync();
            return new GameStoreOutputModel()
            {
                Id = game.Id.ToString(),
                Name = game.Name,
                Cost = store.Cost,
                Source = game.Source,
            };
        }

        /// <summary>
        /// Gets publisher's game by ID.
        /// </summary>
        /// <param name="publisher">Represents publisher's model.</param>
        /// <param name="id">Represents game's ID.</param>
        /// <returns>Returns <see cref="GameStoreOutputModel"/>.</returns>
        public async Task<GameStoreOutputModel> GetGame(PublisherInputModel publisher, string id)
        {
            var isGuid = Guid.TryParse(id, out var guid);

            if (!isGuid)
            {
                throw new NotGuidException();
            }

            var result = await Context.GetGameByGuid(guid);

            if (result == null)
            {
                throw new NotExistingGameException();
            }

            if (publisher.Id == result.Game.PublisherId.ToString())
            {
                return new GameStoreOutputModel()
                {
                    Id = result.Id.ToString(),
                    Name = result.Game.Name,
                    Cost = result.Cost,
                    Source = result.Game.Source,
                };
            }
            else
            {
                throw new ForbiddenException();
            }
        }
    }
}
