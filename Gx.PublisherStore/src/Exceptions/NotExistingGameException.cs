namespace GxPublisherStore.Exceptions
{
    using System.Net;

    /// <summary>
    /// 404: A game has not found in database. HTTP exception class.
    /// </summary>
    public class NotExistingGameException : HttpException
    {
        /// <summary>
        /// Initializes a new instance of the <see cref="NotExistingGameException"/> class.
        /// </summary>
        /// <param name="detail">Context of the error.</param>
        /// <param name="detailVisible">Flag indicates that detail should be returned in the response.</param>
        public NotExistingGameException(object detail = null, bool detailVisible = false)
            : base(
                "Game has not found.",
                HttpStatusCode.NotFound,
                detail,
                detailVisible)
        {
        }
    }
}
