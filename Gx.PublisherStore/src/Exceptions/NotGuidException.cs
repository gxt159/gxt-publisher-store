namespace GxPublisherStore.Exceptions
{
    using System.Net;

    /// <summary>
    /// 400: Guid in request has invalid structure. HTTP exception class.
    /// </summary>
    public class NotGuidException : HttpException
    {
        /// <summary>
        /// Initializes a new instance of the <see cref="NotGuidException"/> class.
        /// </summary>
        /// <param name="detail">Context of the error.</param>
        /// <param name="detailVisible">Flag indicates that detail should be returned in the response.</param>
        public NotGuidException(object detail = null, bool detailVisible = false)
            : base(
                "Received id is not guid.",
                HttpStatusCode.BadRequest,
                detail,
                detailVisible)
        {
        }
    }
}
