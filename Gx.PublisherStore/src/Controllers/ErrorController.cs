namespace GxPublisherStore.Controllers
{
    using System.Net;
    using GxPublisherStore.Exceptions;
    using Microsoft.AspNetCore.Diagnostics;
    using Microsoft.AspNetCore.Mvc;
    using Newtonsoft.Json;

    /// <summary>
    /// Provides an API for error handling.
    /// </summary>
    [ApiController]
    [Route("[controller]")]
    public class ErrorController : ControllerBase
    {
        /// <summary>
        /// Formats responses for unhandled exceptions for the development mode.
        /// </summary>
        /// <returns>Extended exception response for the development mode.</returns>
        [Route("dev")]
        protected IActionResult ErrorDev()
        {
            var context = HttpContext.Features.Get<IExceptionHandlerFeature>();

            if (!(context.Error is HttpException exception))
            {
                return Problem(
                    detail: context.Error.StackTrace,
                    title: context.Error.Message);
            }

            var detailView = exception.DetailVisible
                ? JsonConvert.SerializeObject(exception.Detail)
                : context.Error.StackTrace;

            return Problem(
                detail: detailView,
                statusCode: (int)(exception.StatusCode ?? HttpStatusCode.InternalServerError),
                title: exception.Title);
        }

        /// <summary>
        /// Formats responses for unhandled exceptions for the production mode.
        /// </summary>
        /// <returns>Production-ready response.</returns>
        protected IActionResult Error()
        {
            var context = HttpContext.Features.Get<IExceptionHandlerFeature>();

            if (!(context.Error is HttpException exception))
            {
                return Problem();
            }

            var detailView = exception.DetailVisible
                ? JsonConvert.SerializeObject(exception.Detail)
                : null;

            return Problem(
                detail: detailView,
                statusCode: (int)(exception.StatusCode ?? HttpStatusCode.InternalServerError),
                title: exception.Title);
        }
    }
}
