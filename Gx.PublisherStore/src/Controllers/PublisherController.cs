namespace GxPublisherStore.Controllers
{
    using System.Collections.Generic;
    using System.Threading.Tasks;
    using GxPublisherStore.Models.Input;
    using GxPublisherStore.Models.Output;
    using GxPublisherStore.Services;
    using Microsoft.AspNetCore.Mvc;

    /// <summary>
    /// Provides an API for handling publisher requests.
    /// </summary>
    [ApiController]
    public class PublisherController : ControllerBase
    {
        /// <summary>
        /// Initializes a new instance of the <see cref="PublisherController"/> class.
        /// </summary>
        /// <param name="publisherService">Publisher account management API provider.</param>
        public PublisherController(IPublisherService publisherService)
        {
            PublisherService = publisherService;
        }

        private IPublisherService PublisherService { get; }

        /// <summary>
        /// Get publisher's games.
        /// </summary>
        /// <param name="publisher">Data including publisher's ID.</param>
        /// <returns>A <see cref="GameStoreOutputModel"/> list representing result of the asynchronous operation.</returns>
        /// <response code="200">Publisher has got games.</response>
        [HttpGet("games")]
        public async Task<List<GameStoreOutputModel>> GetGames([FromQuery] PublisherInputModel publisher)
        {
            return await PublisherService.GetGames(publisher);
        }

        /// <summary>
        /// Create pulisher's game.
        /// </summary>
        /// <param name="model">The model including <see cref="GameInputModel"/> and <see cref="PublisherInputModel"/>.</param>
        /// <returns>A <see cref="GameStoreOutputModel"/> representing result of the asynchronous operation.</returns>
        /// <response code="200">Publisher has created a game successfully.</response>
        [HttpPost("game")]
        public async Task<GameStoreOutputModel> CreateGame([FromBody] PublisherGameInputModel model)
        {
            return await PublisherService.CreateGame(model.Publisher, model.Game);
        }

        /// <summary>
        /// Get publisher's game.
        /// </summary>
        /// <param name="publisher">The model including publisher's ID.</param>
        /// <param name="id">Represents game's ID.</param>
        /// <returns>A <see cref="GameStoreOutputModel"/> representing result of the asynchronous operation.</returns>
        /// <response code="200">Publisher has got game successfully.</response>
        /// <response code="404">Publisher tries to get the non-existing game.</response>
        /// <response code="400">Publisher has sent invalid guid.</response>
        /// <response code="403">Publisher tries to get not him game.</response>
        [HttpGet("game/{id}")]
        public async Task<GameStoreOutputModel> GetGame([FromQuery] PublisherInputModel publisher, [FromRoute] string id)
        {
            return await PublisherService.GetGame(publisher, id);
        }
    }
}
