variables:
  DOTNET_CORE_VERSION: '3.1'
  HELM_KUBECTL_IMAGE_VERSION: '3.4.1'
  PROJECT_PATH: './Gx.PublisherStore'
  APP_PROD_DOMAIN: 'api.gamexchange.store'
  PUBLISH_DIR: '$PROJECT_PATH/bin/Release/netcoreapp$DOTNET_CORE_VERSION/publish'
  SWASHBUCKLE_VERSION: '5.6.2'
  APP_VERSION: 'latest'
  APPSETTINGS_PRIVATE_FILE_PATH: '/appsettings.private.json'

stages:
  - lint # Light-weight check of the code style.
  - dotnet-build # Strict code style check, builds app for the further testing and deploying.
  - test # Testify the application against set of tests from `Gx.Blueprint.Tests`.
  - docker-build # Packs build artifact to the docker image and push it to registry.
  - deploy # Deploy the app to review or to production environment.

# Base deployment job
.deploy:
  image: dtzar/helm-kubectl:${HELM_KUBECTL_IMAGE_VERSION}
  before_script: # Connect to k8s cluster
    - mkdir ~/.kube
    - cp $KUBECONFIG ~/.kube/config
    - kubectl cluster-info
    - cp $APPSETTINGS_PRIVATE $APPSETTINGS_PRIVATE_FILE_PATH

.docker-build:
  stage: docker-build
  image:
    name: gcr.io/kaniko-project/executor:debug
    entrypoint: [ "" ]
  before_script:
    - cd $PROJECT_PATH
    - mkdir -p /kaniko/.docker
    - echo "{\"auths\":{\"$CI_REGISTRY\":{\"username\":\"$CI_REGISTRY_USER\",\"password\":\"$CI_REGISTRY_PASSWORD\"}}}" >
      /kaniko/.docker/config.json

lint:
  stage: lint
  image: mcr.microsoft.com/dotnet/core/sdk:$DOTNET_CORE_VERSION
  script:
    - dotnet tool install -g dotnet-format
    - export PATH="$PATH:/root/.dotnet/tools"
    - dotnet format --check --verbosity diagnostic

test:
  stage: test
  image: mcr.microsoft.com/dotnet/core/sdk:$DOTNET_CORE_VERSION
  script:
    - dotnet test -c Release /p:CollectCoverage=true

# Build docker image via Kaniko (instead of dind)
docker-build-amd64:
  extends: .docker-build
  script:
    - /kaniko/executor
      --context $CI_PROJECT_DIR/$PROJECT_PATH
      --dockerfile images/ci-cd.Dockerfile
      --destination $CI_REGISTRY_IMAGE/$CI_COMMIT_REF_SLUG:$APP_VERSION-amd64
      --build-arg DOTNET_CORE_VERSION=$DOTNET_CORE_VERSION
      --build-arg ASPNET_RUNTIME_VERSION=$DOTNET_CORE_VERSION

docker-build-arm:
  extends: .docker-build
  script:
    - /kaniko/executor
      --context $CI_PROJECT_DIR/$PROJECT_PATH
      --dockerfile images/ci-cd.Dockerfile
      --destination $CI_REGISTRY_IMAGE/$CI_COMMIT_REF_SLUG:$APP_VERSION-arm
      --build-arg DOTNET_CORE_VERSION=$DOTNET_CORE_VERSION
      --build-arg PUBLISH_DIR=$ARM_PUBLISH_DIR
      --build-arg ASPNET_RUNTIME_VERSION=$DOTNET_CORE_VERSION-buster-slim-arm32v7


deploy_prod:
  extends: .deploy
  only:
    - master
  stage: deploy
  environment:
    name: production
    url: https://$APP_PROD_DOMAIN
  script:
    - cd $PROJECT_PATH
    - kubectl create namespace $KUBE_NAMESPACE --dry-run=client -o yaml | kubectl apply -f -
    - kubectl create secret generic gx-publisher-service-private-settings
      --save-config --dry-run=client
      --from-file=$APPSETTINGS_PRIVATE
      -o yaml |
      kubectl apply -f -
    - helm upgrade -i $CI_PROJECT_NAME chart/
      --set image.tag=$APP_VERSION
      --set image.registry=$CI_REGISTRY_IMAGE/$CI_COMMIT_REF_SLUG
      --set domain=$APP_PROD_DOMAIN

deploy_review:
  extends: .deploy
  only:
    - branches
  except:
    - master
  stage: deploy
  environment:
    name: review/$CI_COMMIT_REF_SLUG
    url: https://$CI_COMMIT_REF_SLUG.$APP_PROD_DOMAIN
    on_stop: stop_review_app
    auto_stop_in: 1 week
  script:
    - cd $PROJECT_PATH
    - kubectl create namespace $KUBE_NAMESPACE --dry-run=client -o yaml | kubectl apply -f -
    - kubectl create secret generic gx-publisher-service-private-settings
      --save-config --dry-run=client
      --from-file=$APPSETTINGS_PRIVATE_FILE_PATH
      -o yaml |
      kubectl apply -f -
    - helm upgrade -i $CI_PROJECT_NAME chart/
      --set image.tag=$APP_VERSION
      --set image.registry=$CI_REGISTRY_IMAGE/$CI_COMMIT_REF_SLUG
      --set domain=$CI_COMMIT_REF_SLUG.$APP_PROD_DOMAIN

stop_review_app:
  extends: .deploy
  only:
    - branches
  except:
    - master
  when: manual
  stage: deploy
  environment:
    name: review/$CI_COMMIT_REF_SLUG
    action: stop
  script:
    - kubectl delete namespace $KUBE_NAMESPACE


